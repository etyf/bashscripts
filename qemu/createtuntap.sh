#!/bin/bash
sudo ip tuntap add dev tun0 mode tun
sudo ip tuntap add dev tun1 mode tun
#ip address add 192.168.85.20/24 dev tun0
#ip address add 192.168.85.21/24 dev tun0

sudo ip link set tun0 up
sudo ip link set tun1 up
sudo ip route add 0.0.0.0/0 dev tun0 table 1
sudo ip route add 0.0.0.0/0 dev tun1 table 2
sudo ip rule add from all fwmark 1 table 1
sudo ip rule add from all fwmark 2 table 2
sudo ip route flush cache
