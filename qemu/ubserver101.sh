#!/bin/bash
#macaddress=$(printf 'DE:AD:BE:EF:%02X:%02X\n' $((RANDOM%256)) $((RANDOM%256)))
macaddress=$(printf '0A:AA:BB:CC:01:01\n')
qemu-system-x86_64 -enable-kvm -m 2048M -nographic -hda ubserver101.qcow2 -device e1000,netdev=net0,mac=$macaddress -netdev tap,id=net0,script=/home/domain/profiles/konovalov_ma/dev/qemu/runscript.sh
