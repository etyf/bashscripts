#!/bin/bash
wifi_uuid="wifi_uuid"
while true; do
  wlan0_connection=$(nmcli device |grep -i ^wlan0| awk '{ print $4 }')
  wlan0_state=$(nmcli device |grep -i ^wlan0| awk '{ print $3 }')
  if [[ "$wlan0_connection" != "$wifi_uuid"  &&  "$wlan0_state" != "connected" ]]; then
    nmcli device wifi rescan
    nmcli -f SSID dev wifi
    nmcli device wifi connect $wifi_uuid
#   echo "test"
  fi
  sleep 1m
done

