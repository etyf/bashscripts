filetype plugin indent on "Включает определение типа файла, загрузку соответствующих ему плагинов и файлов отступов
set term=screen-256color
set encoding=utf-8 "Ставит кодировку UTF-8
set nocompatible "Отключает обратную совместимость с Vi
syntax enable "Включает подсветку синтаксиса



"packeg mnager auto install
if empty(glob('.vim/autoload/plug.vim')) "Если vim-plug не стоит
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"plugins
call plug#begin('.vim/bundle')
Plug 'ErichDonGubler/vim-sublime-monokai'
Plug 'blueshirts/darcula'
Plug 'vim-airline/vim-airline'
"Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
call plug#end()

"colorschemes
"colorscheme darcula

"airline - statusbar
let g:airline_powerline_fonts = 1 "Включить поддержку Powerline шрифтов
let g:airline#extensions#keymap#enabled = 0 "Не показывать текущий маппинг
let g:airline_section_z = "\ue0a1:%l/%L Col:%c" "Кастомная графа положения курсора
let g:Powerline_symbols='unicode' "Поддержка unicode
let g:airline#extensions#xkblayout#enabled = 0 "Про это позже расскажу

"font
set guifont=FiraCode:h16 "https://github.com/tonsky/FiraCode/wiki/Installing


