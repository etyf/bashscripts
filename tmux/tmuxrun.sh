#!/bin/bash
#input parametrs
qemu=4
qemurun=4
servers=6
script=4

new_sessions_array=(qemu qemurun servers script)

#create sessions if dont exist
add_new_session=()
for session in ${new_sessions_array[@]};do
    tmux has-session -t $session &> /dev/null
    check_session_exist=$? #zero if exist
    if [ "$check_session_exist" -eq 1 ];then
        add_new_session+=($session)        
    fi
done
for session_add in ${add_new_session[@]};do
    tmux new-session -d -s $session_add
done

sassion_sum=${new_session_array[@]}


#create panes 
for session in ${new_sessions_array[@]};do
  if [[ "$session" == "${new_sessions_array[0]}" && "$(tmux list-panes -t $session|wc -l)" -eq 1 ]];then 
      tmux send-key -t $session tmux\ split-window\ -t\ 1\ -h C-m
      tmux send-key -t $session tmux\ split-window\ -t\ 2\ -v C-m
      tmux send-key -t $session tmux\ split-window\ -t\ 1\ -v C-m
      sleep 0.1
      panes_sum=$(tmux list-panes -t $session|wc -l)
      for pane in $(seq $panes_sum);do
          tmux send-key -t $session.$pane cd\ \~/ C-m
      done
  fi
done

#tmux settings
##Change pane numbers to start with 1 instead of 0
tmux set -g base-index 1
tmux set -g pane-base-index 1
tmux set -g mouse on
tmux set-option -g repeat-time 0
##remap prefix from 'C-b' to 'C-a'
tmux unbind C-b
tmux unbind C-a
tmux set-option -g prefix C-a
tmux bind-key C-a send-prefix
tmux bind-key -r -T prefix w choose-tree -s
#tmux set -g default-terminal "xterm-256color"

#create panes
for session in ${new_sessions_array[@]};do

##qemu session panes
    if [[ "$session" == "${new_sessions_array[0]}" && "$(tmux list-panes -t $session|wc -l)" -eq 1 ]];then 
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -h C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 2\ -v C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -v C-m
        sleep 0.1
        panes_sum=$(tmux list-panes -t $session|wc -l)
        for pane in $(seq $panes_sum);do
            tmux send-key -t $session.$pane cd\ \~/ C-m
        done
    fi

##qemurun session panes   
    if [[ "$session" == "${new_sessions_array[1]}" && "$(tmux list-panes -t $session|wc -l)" -eq 1 ]];then 
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -h C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 2\ -v C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 2\ -v C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -v C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -v C-m
        sleep 0.1
        panes_sum=$(tmux list-panes -t $session|wc -l)
        for pane in $(seq $panes_sum);do
            tmux send-key -t $session.$pane cd\ \~/dev/ C-m
        done
    fi

##script session panes   
    if [[ "$session" == "${new_sessions_array[2]}"  && "$(tmux list-panes -t $session|wc -l)" -eq 1 ]];then 
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -h C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 2\ -v C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -v C-m
        sleep 0.1
        panes_sum=$(tmux list-panes -t $session|wc -l)
        for pane in $(seq $panes_sum);do
            tmux send-key -t $session.$pane cd\ \~/ C-m
        done
    fi

##servers session panes   
    if [[  "$session" == "${new_sessions_array[3]}" && "$(tmux list-panes -t $session|wc -l)" -eq 1 ]];then 
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -h C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 2\ -v C-m
        tmux send-key -t $session tmux\ split-window\ -t\ 1\ -v C-m
        sleep 0.1
        panes_sum=$(tmux list-panes -t $session|wc -l)
        for pane in $(seq $panes_sum);do
            tmux send-key -t $session.$pane cd\ \~/ C-m
        done
   fi

done

#command to all panes
session_list=$(tmux list-session |egrep -o ^.*\:\ |sed 's/://')
for session in ${session_list[@]};do
    panes_sum=$(tmux list-panes -t $session|wc -l)
    for pane in $(seq $panes_sum);do
#       tmux send-key -t $session.$pane PS1=\"\$\(pwd\)\:\$\\n \" C-m
        tmux send-key -t $session.$pane clear C-m
    done
done
