#!/bin/bash
#search user group  in ldap by login

#variables
server_ip='192.168.1.1'
secret_filename='ldap.secret'
ldap_basename='dc=example,dc=com'
ldap_user='cn=user,dc=example,dc=com'

function ldap_group_search_by_name() {
    echo ""
    echo $1
    echo $(ldapsearch -H ldap://$server_ip/ -LLL -y $secret_filename -b $ldap_basename -D $ldap_user "memberUid=$1" dn |grep -o "cn=[^,]*")
}

echo "LDAP group search"
echo "----------"
until [ "$continue_yn" == "n" ] || [ "$continue_yn" == "N" ];do
    echo "enter user"
    read ldap_name
#get all names by input pattern    
unique_names=($(echo $(ldapsearch -H ldap://$server_ip/ -LLL -y $secret_filename -b $ldap_basename -D $ldap_user "memberUid=*" |grep -i $ldap_name |sed 's/memberUid:\ //') |tr ' ' '\n' |sort -u |tr '\n' ' '))
if [ "${#unique_names[@]}" -gt 1 ];then
    echo "Enter menu number"
    for i in ${!unique_names[@]};do
        echo $(($i+1)) - ${unique_names[$i]}
    done
    read menu_number
    ((menu_number--))

    ldap_group_search_by_name ${unique_names[$menu_number]}
elif [ "${#unique_names[@]}" -eq 0 ];then
    echo "no such user"
else
    ldap_group_search_by_name ${unique_names[@]}
fi
echo ""
echo "continue ? y/n"
echo "----------"
read continue_yn
done

