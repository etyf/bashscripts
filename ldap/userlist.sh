#!/bin/bash
#usernames, hostnames to files
adminlogin='adminlogin'
secretfile=nohistory.secret
nmap_port=22

netmask_office_1='192.168.10.0/24'
netmask_office_2='192.168.20.0/24'
netmask_office_3='192.168.30.0/24'

net_office_1=${netmask_office_1[@]:0:9}'[0-9]*'
net_office_2=${netmask_office_2[@]:0:9}'[0-9]*'
net_office_3=${netmask_office_3[@]:0:9}'[0-9]*'

echo "Enter 1,2 or 3"

#Menu
select NETS in OFFICE1 OFFICE2 OFFICE3; do
   if [ "$NETS" = "OFFICE1" ];then
       IPS=($(nmap $netmask_office_1 -p$nmap_port --open |grep -o $net_office_1))
       arpip=${net_office_1[@]:0:9}
       outputfile=outputfile_office1
       break
   elif [ "$NETS" = "OFFICE2" ];then 
       IPS=($(nmap $netmask_office_2 -p$nmap_port --open| grep -o $net_office_2))
       arpip=${net_office_2[@]:0:9}
       outputfile=outputfile_office2
       break
   elif [ "$NETS" = "OFFICE3" ];then 
       IPS=($(nmap $netmask_office_3 -p$nmap_port --open| grep -o $net_office_3))
       arpip=${net_office_3[@]:0:9}
       outputfile=outputfile_office3
       break

   else
       break
   fi
done

#Erase outputfile
echo "" |tee $outputfile
echo "$(date)" |tee $outputfile

#Linux hosts
echo "" |tee -a $outputfile
echo "------Linux hosts------" |tee -a $outputfile
for i in ${IPS[@]}; do
    get_hostname_users_inarray=($(sshpass -f $secretfile ssh -o StrictHostKeyChecking=no $adminlogin@$i 'echo $(hostname) $(users)' 2>/dev/null))
    get_hostname=${get_hostname_users_inarray[0]}
    get_unique_username=($(echo "${get_hostname_users_inarray[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
#    get_group
    if [ -n "$get_hostname_users_inarray" ]; then
        if [ "$get_unique_username" == "$get_hostname" ];then namescheck="";else namescheck=" !";fi 
        echo $i - USERS: $get_unique_username \| HOSTNAME: $get_hostname$namescheck | tee -a $outputfile
        post_unique_array+=($get_unique_username)
    fi
done

#ARP scan
echo "" | tee -a $outputfile
echo "------ARP scan------" |tee -a $outputfile

for i in {1..254};do
    a=$(arp $arpip$i |grep ^[a-z,A-Z] |grep ether)
    if [ -n "$a" ];then
        echo $arpip$i - $a |tee -a $outputfile
    else
        continue
    fi
done

